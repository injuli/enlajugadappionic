import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UsersProvider } from '../../providers/users/users';
import { HomePage } from '../home/home';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  username: any;
  email: any;
  password: any;
  age: any;
  sex: any = 1;
  phone: any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, private usersService: UsersProvider) {
  }

  ionViewDidLoad() {
    /*this.usersService.get()
    .subscribe((r)=>{
      console.log(r);
    });*/
  }

  registrar(){
    this.usersService.create({
      username: this.username,
      email: this.email,
      password: this.password,
      age: this.age,
      sex: this.sex,
      phone: this.phone
    }).subscribe((data)=>{
      if(data["error"] == 0){
        //TODO: hacer el local storage
        this.navCtrl.setRoot(HomePage);
      }
    });
  }

}
