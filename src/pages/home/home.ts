import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReportDetailPage } from '../report-detail/report-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  title: string = "Home";
  reportDetail: any = ReportDetailPage;
  
  items: any = [
    { id: 1,tipoRobo: "Robo de dignidad", lugar: "McDonalds Cañas gordas" },
    { id: 2, tipoRobo: "Robo de dignidad", lugar: "McDonalds Cañas gordas" },
    { id: 3, tipoRobo: "Robo de dignidad", lugar: "McDonalds Cañas gordas" }
  ]
  constructor(public navCtrl: NavController) {

  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 3; i++) {
        this.items.push( { tipoRobo: "Robo de dignidad", lugar: "McDonalds Cañas gordas" } );
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

}
